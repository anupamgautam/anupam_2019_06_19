function generateCanvas() {
	document.getElementById("wrappper").innerHTML = "";
	var select = document.getElementById("select");
	select.options.length = 0;
	var nos = Math.floor(Math.random() * 3)+2;
	document.getElementById("nos").innerHTML = nos;
	
	for(var i = 1; i <= nos; i++ ) {
		var canvas = document.createElement('canvas');
		canvas.id = "canvasLayer"+i;
		canvas.width = 200;
		canvas.height = 200;
		canvas.style.border = "1px solid #000";
		canvas.style.margin = "10px";
		var wrappper = document.getElementById("wrappper");
		wrappper.appendChild(canvas);
		
		var option = document.createElement("option");
		option.text = "Canvas "+i;
		option.value = "canvasLayer"+i;
		select.appendChild(option);
	}
	
	getImageSrc();
}

function getImageSrc() {
	var srcObj = new Promise(function(resolve,reject){
    let req = new XMLHttpRequest();
    req.open('GET', 'https://jsonplaceholder.typicode.com/photos');
 
    req.onload = function() {
      req.status == 200 ? resolve(req.response): reject(req.response);
    };
    req.onerror = function() {
      reject(Error('Error requesting data'));
    };
    req.send();
  });
  
  srcObj.then(function(response) {
	  var data = JSON.parse(response);
	  window.requestObject = data;
	  //var firstTwoEle = (data.slice(0,2));
	  //var lastTwoEle = (data.slice(Math.max(data.length - 2, 1)));
	  //var randNum = Math.floor(Math.random() * 4999); 
	  //var randEle = data[randNum];
	}, function(error) {
	  console.error(error);
	})
}



